const functions = require("firebase-functions");
const admin = require('firebase-admin');
admin.initializeApp();

const db = admin.firestore();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

exports.useWildcard = functions.https.onRequest(async (req, res) => {
    var user= db.doc(req.query.user ?? 'user/NVulBnd3NhTCMVyFqYzAKaCt1H73' );
    var restaurant =  db.doc(req.query.restaurant ??'restaurant/sk2e4elWWK9cjJ3JN5O8' );
    var datum = new Date(Date.UTC(req.query.year?? '2021', req.query.moth?? '01',req.query.date?? '13', req.query.hours?? '12',req.query.minutes??'31',req.query.seconds??'30'));
    var timestamp = admin.firestore.Timestamp.fromDate(datum);
    var repeat = req.query.repeat ? parseInt(req.query.repeat):10;
    var difference = req.query.difference ? parseInt(req.query.difference):30;
    for (const i of Array(repeat).keys() ) 
    {
        console.log(datum.toString()+"BEFOR");
        datum = new Date(datum.getTime() + difference*60000);
        console.log(datum.toString()+"DATE");

        datfi = new Date(datum.getTime() + 15*60000);
        console.log(datfi.toString()+"fi");
        timestamp = admin.firestore.Timestamp.fromDate(datum);
        timestampfi = admin.firestore.Timestamp.fromDate(datfi);
        await db.collection('delivery').add({
            clientName: 'Juanita',
            deliveryDestination: {
                address:"Country club",
                coordinate : new admin.firestore.GeoPoint(4.710493684827736, -74.04221545820411)
            },
            deliveryOrigin: {
                address:"Platino",
                coordinate : new admin.firestore.GeoPoint(4.716097943124383, -74.02923867202588)
            },
            deliveryImage: null,
            finishDate: timestampfi,
            orderID: Math.floor(Math.random() * 10000000).toString(),
            restaurant: restaurant,
            startDate: timestamp,
            status:2,
            phone: "33332423423",
            user: user
          });
           console.log("llegue2");
          
    }
    res.status(200).send("correct");
});

exports.changeAll = functions.https.onRequest(async (req, res) => {
    var deliveries= await db.collection('delivery').get();
    let allProducts = ["Pineapple Pizza", "Chocolate Ice Cream", "Fried Rice", "Orange Juice", "Hamburger", "Cheesecake"];
    let will = [true, false];
    

    deliveries.forEach(i=>
    {
       console.log(i.id);
       db.collection('delivery').doc(i.id).update({
        product: allProducts[Math.floor(Math.random() * allProducts.length)] ,
        willPayOnDelivery: will[Math.floor(Math.random() * will.length)] ,

      });
        
    });
    res.status(200).send("correct");
});

exports.averageTimeDelivery = functions.https.onRequest(async (req, res) => {
    const user = req.query.user?? 'NVulBnd3NhTCMVyFqYzAKaCt1H73';
    const deliveriesRef = db.collection('delivery');
    const snapshot = await deliveriesRef.where('user', '==', db.doc('user/' + user)).get();
    if (snapshot.empty) {
        console.log('No matching documents.');
        res.send("0");
        return;
    }
    let dates = [];
    snapshot.forEach(doc => {
        dates.push(doc.data().startDate.toDate());
    });
    const datesSort = dates.sort();
    let ant = "";
    let cuantos = 0;
    let sumDif = 0;
    let average = 0;
    let differences = [];
    for (date of datesSort) {
        if (ant !== "" && date.toLocaleDateString() === ant.toLocaleDateString()) {
            let dif = ((date.getTime() - ant.getTime())) / (1000 * 60);
            differences.push(dif);
        } else {
            ant = date;
        }
    }
    differences = differences.sort(function(a, b) {
        return a - b;
      });
    let tam =  differences.length;
    let c25 = differences[Math.floor(tam*0.25)];
    let c75 = differences[Math.floor(tam*0.75)];
    let resta = Math.abs( c25 - c75)*1.5;
    let mediana = tam % 2 === 1 ? differences[Math.floor(tam/2)]: ((differences[Math.floor(tam/2)]+ differences[Math.floor(tam/2)+1])/2);
    for(diff of differences)
    {
        console.log(diff);
        if (mediana + resta >=diff && mediana - resta <=diff  )
        {
            sumDif += diff;
            cuantos +=1;
        }else{
            console.log("WITHOUT "+diff);
        }
    }
    if (cuantos !== 0)
    {
        average = (sumDif / cuantos);
    }else
    {
        console.log('No exist enough data.');
        res.send("0");
        return;
    }
    res.status(200).send(average.toString());
});